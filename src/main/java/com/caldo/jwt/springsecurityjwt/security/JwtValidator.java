package com.caldo.jwt.springsecurityjwt.security;

import com.caldo.jwt.springsecurityjwt.model.JwtUser;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class JwtValidator {

    private String secret = "youtube";

    public JwtUser validate(String token) {

        JwtUser jwtUser = null;

        try {
            Claims body = Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody();

            jwtUser = new JwtUser()
                    .setId(Long.parseLong((String) body.get("userId")))
                    .setUserName(body.getSubject())
                    .setRole((String) body.get("role"));
        } catch (Exception e) {
            log.info("Error on :: " + getClass() + " " + e);
        }

        return jwtUser;
    }
}
