package com.caldo.jwt.springsecurityjwt.security;

import com.caldo.jwt.springsecurityjwt.model.JwtUser;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Service;

@Service
public class JwtGenerator {


    public static final String YOUTUBE = "youtube";

    public String generate(JwtUser jwtUser) {

        // per ficar data de caducitat al token podem fer servir el metode Jwts.builder().setExpiration()
        Claims claims = Jwts.claims()
                .setSubject(jwtUser.getUserName());
        claims.put("userId", String.valueOf(jwtUser.getId()));
        claims.put("role", jwtUser.getRole());


        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, YOUTUBE)
                .compact();
    }
}
