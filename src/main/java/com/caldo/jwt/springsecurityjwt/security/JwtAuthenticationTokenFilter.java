package com.caldo.jwt.springsecurityjwt.security;

import com.caldo.jwt.springsecurityjwt.model.JwtAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JwtAuthenticationTokenFilter extends AbstractAuthenticationProcessingFilter {

    public static final String JWT_TOKEN_IS_MISSING = "JWT Token is missing";

    /**
     *
     */
    public JwtAuthenticationTokenFilter() {
        super("/rest/**");
    }

    /**
     * Parece ser que en esta clase ira implementada la logica de validacion del token.
     *
     * @param request
     * @param response
     * @return
     * @throws AuthenticationException
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {

        final String header = request.getHeader("Authorisation");

        if (header == null) {
            throw new RuntimeException(JWT_TOKEN_IS_MISSING);
        }

        final String authenticationToken = header.substring(0);
        JwtAuthenticationToken token = new JwtAuthenticationToken(authenticationToken);

        return getAuthenticationManager().authenticate(token);
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        super.successfulAuthentication(request, response, chain, authResult);
        chain.doFilter(request, response);
    }
}
