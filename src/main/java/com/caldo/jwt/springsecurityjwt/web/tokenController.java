package com.caldo.jwt.springsecurityjwt.web;

import com.caldo.jwt.springsecurityjwt.model.JwtUser;
import com.caldo.jwt.springsecurityjwt.security.JwtGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/token")
public class tokenController {

    @Autowired
    private JwtGenerator jwtGenerator;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public String generateToken(@RequestBody @Valid final JwtUser jwtUser) {
        return this.jwtGenerator.generate(jwtUser);
    }
}
