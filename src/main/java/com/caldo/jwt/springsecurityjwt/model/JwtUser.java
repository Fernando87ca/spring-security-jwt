package com.caldo.jwt.springsecurityjwt.model;

import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

@Data
@Accessors(chain = true)
public class JwtUser {

    @NotNull
    private long id;

    @NotNull
    @NotEmpty
    private String userName;

    private String role;
}
