package com.caldo.jwt.springsecurityjwt.model;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

@Data
@Accessors(chain = true)
public class JwtUserDetails implements UserDetails {

    private long id;
    private String username;
    private String token;
    private Collection<? extends GrantedAuthority> grantedAuthorities;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.grantedAuthorities;
    }

    @Override
    public String getPassword() {
        return null;
    }

    /**
     * Estos de debajo configuran el tipo de conexion que vamos a dar, ex
     * isAccountNonLocked: la cuenta no esta bloqueada
     * isAccountNonExpired: la cuenta no ha expirado.
     *
     * Al iniciarla lo tenemos que crear asi, luego podemos cambiarlo para bloquearlo o historias.
     *
     * @return
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
